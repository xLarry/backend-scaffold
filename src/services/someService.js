let state = {
	someState: undefined
}

export default {
  updateState(newState) {
    state = newState
  },

	getState() {
		return state
	}
}