import postgres from 'postgres'
import config from '../config'

const db = postgres({
	host: config.db.host,
	port: config.db.port,
	username: config.db.username,
	password: config.db.password
})

export default db