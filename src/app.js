import express from 'express'
import cors from 'cors'
import config from './config'
import apiRoutes from './api'

const app = express()

// CORS
app.use(cors())

// Body parser
app.use(express.json())

// Import routers
app.use('/api', apiRoutes)

app.listen(config.app.port, config.app.host, () => {
  console.log(`Server is running on http://localhost:${config.app.port}`)
})
