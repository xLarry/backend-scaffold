import { Router } from 'express'
import itemRoutes from './items'

const routes = Router()

routes.use('/items', itemRoutes)

export default routes
