import { Router } from 'express'
import * as controller from './item.controller'

const routes = Router()

routes.get('/', controller.getItems)

export default routes
