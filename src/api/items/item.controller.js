import db from '../../services/database'

const getItems = async (req, res) => {

	const items = await db`
		SELECT * FROM items
	`
	res.json(items)
}

export { getItems }
