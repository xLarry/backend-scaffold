const config = {
	app: {
		port: 3000,
		url: "http://localhost:3000",
	},
	db: {
		host: process.env.DB_HOST || 'localhost',
		port: process.env.DB_PORT || 5432,
		username: process.env.DB_USER,
		password: process.env.DB_PASSWORD
	}
}
   
export default config