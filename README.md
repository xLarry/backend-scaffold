# backend-scaffold

## Overview

- The server is based on the following libraries:
	- [express](https://github.com/expressjs/express) (Routing)
	- [postgres](https://github.com/porsager/postgres) (DB Client)
	- [ley](https://github.com/lukeed/ley) (DB Migration)
	- [webpack](https://github.com/webpack/webpack) (Build)

## Getting Started

### Initialize

- Rename `.env.sample` to `.env` and configure your environment
- Configure your database schema in `./migrations/001_create-schema.js`
- Run `npm run db:init` to initialize the database

### Develop

- Run `docker-compose up`
- The current working directory will be mounted into the container to enable live reloading

### Deploy

- Run `docker-compose -f docker-compose.yml -f docker-compose.prod.yml up`