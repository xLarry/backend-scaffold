const fs = require('fs')
const dotenv = require('dotenv')

const envConfig = dotenv.parse(fs.readFileSync('.env'))

for (const k in envConfig) {
  process.env[k] = envConfig[k]
}

process.env.PGUSERNAME = process.env.DB_USER
process.env.PGPASSWORD = process.env.DB_PASSWORD
