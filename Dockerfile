FROM node:14

# Use dumb-init to run node with PID 1
RUN apt update && apt install dumb-init

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./

RUN npm install
# Use the following in PROD
# RUN npm ci --only=production

# Bundle app source
COPY . .

# Build the app
RUN npm run build

# Make sure this matches the configured port in ./config/index.js
EXPOSE 3000

CMD [ "dumb-init", "npm", "run", "watch" ]